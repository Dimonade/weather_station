
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"
#include <SeeedGrayOLED.h>

#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME680 bme680; // I2C

const int pinLight = A0;

int lightValue;
float temperatureValue;
float humidityValue;
float pressureValue;
float vocValue;

String measurements[] = {"Temperature", "Pressure", "Humidity", "VOC"};
int measurementsSize = (sizeof(measurements) / sizeof(measurements[0]));


boolean newData = false;
const byte numChars = 40;
char receivedChars[numChars];
char tempChars[numChars];
char command[numChars] = {0};

void setup(){
  Wire.begin();
  SeeedGrayOled.init(SH1107G);      //initialize SEEED OLED display
  SeeedGrayOled.clearDisplay();     //Clear Display.
  SeeedGrayOled.setNormalDisplay(); //Set Normal Display Mode
  SeeedGrayOled.setVerticalMode();  // Set to vertical mode for displaying text
  SeeedGrayOled.setGrayLevel(0); //Set Grayscale level. Any number between 0 - 15.
  while (!bme680.begin()) {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
    while (1);
  }
  Serial.begin(9600);

  // Set up oversampling and filter initialization
  bme680.setTemperatureOversampling(BME680_OS_8X);
  bme680.setHumidityOversampling(BME680_OS_2X);
  bme680.setPressureOversampling(BME680_OS_4X);
  bme680.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme680.setGasHeater(320, 150); // 320*C for 150 ms
}

void loop(){
  recvWithSTRTEndMarkers();
  if (newData == true) {
    strcpy(tempChars, receivedChars);
    // this temporary copy is necessary to protect the original data
    // because strtok() used in parseData() replaces the commas with \0
    parseData();

    if (strcmp(command, "GetAllMeasurements") == 0){
      GetAllMeasurements();
    }
    else if (strcmp(command, "GetMeasurementsNames") == 0){
      GetMeasurementsNames();
    }
    newData = false;
  }
}

//============

void recvWithSTRTEndMarkers() {
  static boolean recvInProgress = false;
  static byte ndx = 0;
  char startMarker = '<';
  char endMarker = '>';
  char rc;

  while (Serial.available() > 0 && newData == false) {
    rc = Serial.read();

    if (recvInProgress == true) {
      if (rc != endMarker) {
        receivedChars[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      }
      else {
        receivedChars[ndx] = '\0'; // terminate the string
        recvInProgress = false;
        ndx = 0;
        newData = true;
      }
    }
    else if (rc == startMarker) {
      recvInProgress = true;
    }
  }
}

//============

void parseData() {      // split the data into its parts.
  char * strtokIndx; // this is used by strtok() as an index.

  strtokIndx = strtok(tempChars, ",");     // get the first part - the string.
  strcpy(command, strtokIndx); // copy it to command.
}

//============

void GetMeasurementsNames() {
  Serial.print("<");
  for (int m=0; m<measurementsSize; m++) {
    Serial.print(measurements[m]);
    if (m != measurementsSize-1){
      Serial.print(",");
    }
    else {
      Serial.print(">");
      Serial.println();
    }
  }
  
}


void GetAllMeasurements() {
  
  lightValue = analogRead(pinLight);
  bme680.performReading();
  temperatureValue = bme680.temperature;
  pressureValue = bme680.pressure/1000.0;
  humidityValue = bme680.humidity;
  vocValue = bme680.gas_resistance/1000.0;

  Serial.print("<");
  // Currently there is no embedded light sensor, only external.
  //Serial.print(lightValue);
  //Serial.print(",");
  Serial.print(temperatureValue);
  Serial.print(",");
  Serial.print(pressureValue);
  Serial.print(",");
  Serial.print(humidityValue);
  Serial.print(",");
  Serial.print(vocValue);
  Serial.print(">");
  Serial.println();
  
  SeeedGrayOled.setTextXY(0,0);
  SeeedGrayOled.putString("Env. Station <3 ");
  SeeedGrayOled.setTextXY(1,0);
  SeeedGrayOled.putString("Light: ");
  SeeedGrayOled.putNumber(lightValue);
  SeeedGrayOled.setTextXY(2,0);
  SeeedGrayOled.putString("Temp: ");
  SeeedGrayOled.putNumber(temperatureValue);
  SeeedGrayOled.setTextXY(3,0);
  SeeedGrayOled.putString("Pressure: ");
  SeeedGrayOled.putNumber(pressureValue);
  SeeedGrayOled.setTextXY(4,0);
  SeeedGrayOled.putString("Humidity: ");
  SeeedGrayOled.putNumber(humidityValue);
  SeeedGrayOled.setTextXY(5,0);
  SeeedGrayOled.putString("VOC: ");
  SeeedGrayOled.putNumber(vocValue);
  delay(5000);
  SeeedGrayOled.clearDisplay();     //Clear Display.
  
  Serial.flush();
}
