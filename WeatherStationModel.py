import os
import psutil
import time
import datetime
import re
import serial
import serial.tools.list_ports
import pandas as pd
import numpy as np

from WeatherStation import platform_type
from PyQt5.QtCore import QThread, pyqtSignal


class ScanDevicesThread(QThread):
    devicesSignal = pyqtSignal(dict)
    
    def __init__(self, preferences, 
                 updatePlotsSlot):
        super(ScanDevicesThread, self).__init__()
        self.devicesSignal.connect(updatePlotsSlot)
        
        self.preferences = preferences
        return
    
    def UpdateDevices(self, preferences):
        self.devicesSignal.emit(preferences)
        return
    
    
    def run(self):
        coms = list(serial.tools.list_ports.comports())
        
        if coms != list([]):
            if "Windows" in platform_type:
                for i, com in enumerate(coms):
                    if "Arduino" in coms[i].manufacturer or "Arduino" in coms[i].description:
                        self.preferences["Devices"].append(coms[i].description)
            
            elif "Linux" in platform_type:
                for i, com in enumerate(coms):
                    try:
                        if "Arduino" in coms[i].manufacturer or \
                            "Arduino" in coms[i].description or \
                            "FTDI" in coms[i].manufacturer:  # This is the recongnized Nano name.
                            self.preferences["Devices"].append("{} ({})".format(coms[i].description, coms[i].device))
                    except:
                        print("COM issue, skipping to next COM port.")
            else:
                print("Unknown platform, update the WeatherStation.py file for new platform.")
    
        elif self.preferences["Devices"] == list([]):
            print("Did not find Arduino microcontrollers.")
        
        self.UpdateDevices(self.preferences)
        
        return



class GetMeasurementsThread(QThread):
    measurementReadySignal = pyqtSignal()
    
    def __init__(self, preferences, measurementReadySlot):
        super(GetMeasurementsThread, self).__init__()
        
        self.preferences = preferences
        self.isPaused = False
        self.measurementReadySignal.connect(measurementReadySlot)
        self.timer = dict({"PlotUpdate":float(self.preferences["Preferences"].get("General", "plot_update")),
                           "LastUpdate":time.time(),
                           "TimePassed":0})
        return
    
    
    def PauseUnpause(self, isPaused):
        print("Let's try some wizardry...")
        self.isPaused = isPaused
        return
    
    
    def PlotTimer(self):
        print("Checking timer.")
        self.timer["TimePassed"] = time.time() - self.timer["LastUpdate"]
        
        if self.timer["TimePassed"] < self.timer["PlotUpdate"]:
            print("Timer not ready. Passed: {}. Required: {}.".format(self.timer["TimePassed"], self.timer["PlotUpdate"]))
            time.sleep(1)
            return False
        else:
            print("Timer is ready. Updating the plot.")
            self.timer["LastUpdate"] = time.time()
            return True
    
    
    def SendAndReceiveMessage(self, mc, to_mc, tries=3):
        from_mc = str("")
        
        mc.write((to_mc).encode())
        print("Sending the {} command.".format(to_mc))
        time.sleep(3)
        from_mc = mc.readline().decode()
        
        if ">" in from_mc:
            return from_mc
        else:
            print("Did not receive the end token.")
            raise NotImplementedError
    
    
    def InitiateFile(self, mc, folder_path):
        # Temporary initiation with a minute resolution to test creation of new files.
        file_shape = self.preferences["Preferences"].get("General", "file_shape")
        file_path = os.path.join(folder_path, "{}.csv".format(datetime.datetime.now().strftime(file_shape)[1:-1]))
        
        # Check if a file with the same time deviation and column names already exists.
        measurements_names = self.SendAndReceiveMessage(mc, "<GetMeasurementsNames>")
       
        column_names = measurements_names.partition("<")[2].partition(">")[0]
        column_names = "DateTime,{},CPU_Temperature".format(column_names)
        print("Received the following measurement names: {}.".format(column_names))
        cols_new_separated = re.findall("[\w]+", column_names)
        
        if os.path.isfile(file_path):
            # File exists, check if columns align.
            print("File exists, checking if column names align.")
            with open(file_path) as f:
                existing_columns = f.readline()
                cols_existing_separated = re.findall("[\w]+", existing_columns)
            print("Existing columns:\n{}\nNew columns:\n{}".format(cols_existing_separated, cols_new_separated))
            
            if cols_existing_separated == cols_new_separated:
                print("File exists, columns are equivalent, will append to current file.")
                return file_path
            else:
                print("File exists, columns are not equivalent, will append to a new (conflicted) file.")
                file_path = "{}_conflicted.csv".format(file_path[:-4])
                with open(os.path.join(file_path), "a") as mb:
                    mb.write(column_names)
                    mb.write("\n")
                return file_path
        
        
        else:
            # File does not exist, create anew.
            print("A file does not exists. Creating with the listed measurement names.")
            with open(os.path.join(file_path), "a") as mb:
                mb.write(column_names)
                mb.write("\n")
            return file_path
    
    
    def ConnectMicrocontrollers(self):
        if "Windows" in platform_type:
            dev00com = re.search(r"\((.*?)\)", self.preferences["Preferences"].get("Device00", "com")).group(1)
        elif "Linux" in platform_type:
            dev00com = re.search(r"\((.*?)\)", self.preferences["Preferences"].get("Device00", "com")).group(1)
        else:
            print("Unknown path to device.")
        
        try:
            print("Trying to connect to microcontroller.")
            mc = serial.Serial(dev00com, baudrate=9600, timeout=1, writeTimeout=0)
        except serial.SerialException:
            print("Microcontroller's COM is open, restart the microcontroller and run again.")
        time.sleep(2)
        
        return mc
    
    
    def AcquireCpuTemperature(self):
        if "Windows" in platform_type:
            cpu_temp = -1
        elif "Linux" in platform_type:
            cpu_temp = psutil.sensors_temperatures()["coretemp"][0].current
        
        
        return cpu_temp
    
    
    def run(self):
        if len(self.preferences["Devices"]) <= 0:
            print("Could not find devices.")
            return
        
        # Creating the microcontroller object.
        mc = self.ConnectMicrocontrollers()
        
        
        
        
        # Create path, if exists, ignore.
        dev_00_folder_path = os.path.join(self.preferences["AcquiredDataPath"], 
                                    self.preferences["Preferences"].get("Device00", "name"))
        os.makedirs(dev_00_folder_path, exist_ok=True)
        print("Created acquired data path folder for device00.")
        
        dev_00_path = self.InitiateFile(mc, dev_00_folder_path)
        print("DEV 00 PATH")
        print(dev_00_path)
        self.preferences["AcquiredDataPath"] = dev_00_path
        current_time_index = re.findall("[\d]+", dev_00_path)[-1]
        
        time.sleep(5)
        print("Slept through initiation.")
        while not self.isPaused:
            try:
                measurements = self.SendAndReceiveMessage(mc, "<GetAllMeasurements>")
                
                new_data = measurements.partition("<")[2].partition(">")[0]
                
                new_measurement_time = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
                new_data = "{},{}".format(new_measurement_time, new_data)
                cpu_temp = self.AcquireCpuTemperature()
                new_data = "{},{}".format(new_data, cpu_temp)
                print("Acquired response from microconroller.")
                
                # TODO: This is currently hardcoded to HOURS, will need to change accordingly to the latest digit.
                new_time_index = re.findall("[\d]+", new_measurement_time)[-3]
                if current_time_index != new_time_index:
                    print("Current time deviation changed, updating to create a new file.")
                    dev_00_path = self.InitiateFile(mc, dev_00_folder_path)
                    self.preferences["AcquiredDataPath"] = dev_00_path
                    current_time_index = new_time_index
                
                
                with open(dev_00_path, "a") as mb:
                    mb.write(new_data)
                    mb.write("\n")
                    print("Updated the data file.")
                    
                time.sleep(5)
            except KeyboardInterrupt:
                break
            except serial.SerialException:
                continue
            
            if self.PlotTimer():
                self.measurementReadySignal.emit()
        mc.close()
        mc.__exit__()
        mc.__del__()
        print("Microcontroller is closed: {}.".format(mc.closed))
            
        
# =============================================================================
#         if len(self.preferences["Devices"]) > 0:
#             device01Path = os.path.join(self.preferences["AcquiredDataPath"], 
#                                         self.preferences["Preferences"].get("Device01", "name"))
#             os.makedirs(device01Path, exist_ok=True)
#             print("Created acquired data path for device01.")
# =============================================================================
 
         

        
        return
    
    


class WeatherStationModel(object):
    def setView(self, ui):
        self.ui = ui
        return
    
    def VM_ScanDevices(self, preferencesSlot, preferences):
        self.ScanDevicesThreadInstance = ScanDevicesThread(preferencesSlot, preferences)
        self.ScanDevicesThreadInstance.start()
        return
    
    def VM_GetMeasurements(self, preferences, measurementReadySlot):
        self.GetMeasurementsThreadInstance = GetMeasurementsThread(preferences, measurementReadySlot)
        self.GetMeasurementsThreadInstance.start()
        return

























