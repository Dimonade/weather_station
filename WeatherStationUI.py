# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\WeatherStationUI.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("_icons/main_window.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.tabPlots = QtWidgets.QTabWidget(self.centralwidget)
        self.tabPlots.setObjectName("tabPlots")
        self.tabDevice00 = QtWidgets.QWidget()
        self.tabDevice00.setObjectName("tabDevice00")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.tabDevice00)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.glayDevice00 = QtWidgets.QGridLayout()
        self.glayDevice00.setObjectName("glayDevice00")
        self.widPlotDevice00 = QtWidgets.QWidget(self.tabDevice00)
        self.widPlotDevice00.setObjectName("widPlotDevice00")
        self.glayDevice00.addWidget(self.widPlotDevice00, 0, 0, 1, 1)
        self.gridLayout_4.addLayout(self.glayDevice00, 0, 0, 1, 1)
        self.tabPlots.addTab(self.tabDevice00, "")
        self.tabDevice01 = QtWidgets.QWidget()
        self.tabDevice01.setObjectName("tabDevice01")
        self.tabPlots.addTab(self.tabDevice01, "")
        self.tabSystem = QtWidgets.QWidget()
        self.tabSystem.setObjectName("tabSystem")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.tabSystem)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.glaySystem = QtWidgets.QGridLayout()
        self.glaySystem.setObjectName("glaySystem")
        self.widPlotSystem = QtWidgets.QWidget(self.tabSystem)
        self.widPlotSystem.setObjectName("widPlotSystem")
        self.glaySystem.addWidget(self.widPlotSystem, 0, 0, 1, 1)
        self.gridLayout_2.addLayout(self.glaySystem, 0, 0, 1, 1)
        self.tabPlots.addTab(self.tabSystem, "")
        self.gridLayout.addWidget(self.tabPlots, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        self.menuAcquision = QtWidgets.QMenu(self.menubar)
        self.menuAcquision.setObjectName("menuAcquision")
        self.menuSettings = QtWidgets.QMenu(self.menubar)
        self.menuSettings.setObjectName("menuSettings")
        MainWindow.setMenuBar(self.menubar)
        self.actionStart_acquision = QtWidgets.QAction(MainWindow)
        self.actionStart_acquision.setCheckable(True)
        self.actionStart_acquision.setObjectName("actionStart_acquision")
        self.actionStop_acquision = QtWidgets.QAction(MainWindow)
        self.actionStop_acquision.setObjectName("actionStop_acquision")
        self.actionStart_stop_acquistion = QtWidgets.QAction(MainWindow)
        self.actionStart_stop_acquistion.setCheckable(True)
        self.actionStart_stop_acquistion.setObjectName("actionStart_stop_acquistion")
        self.actionPreferences = QtWidgets.QAction(MainWindow)
        self.actionPreferences.setObjectName("actionPreferences")
        self.menuAcquision.addAction(self.actionStart_stop_acquistion)
        self.menuSettings.addAction(self.actionPreferences)
        self.menubar.addAction(self.menuAcquision.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())

        self.retranslateUi(MainWindow)
        self.tabPlots.setCurrentIndex(2)
        self.actionStart_stop_acquistion.triggered.connect(MainWindow.StartStopAcquisition)
        self.actionPreferences.triggered.connect(MainWindow.DisplayPreferences)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Weather Station"))
        self.tabPlots.setTabText(self.tabPlots.indexOf(self.tabDevice00), _translate("MainWindow", "Device00"))
        self.tabPlots.setTabText(self.tabPlots.indexOf(self.tabDevice01), _translate("MainWindow", "Device01"))
        self.tabPlots.setTabText(self.tabPlots.indexOf(self.tabSystem), _translate("MainWindow", "System"))
        self.menuAcquision.setTitle(_translate("MainWindow", "Acquision"))
        self.menuSettings.setTitle(_translate("MainWindow", "Settings"))
        self.actionStart_acquision.setText(_translate("MainWindow", "Start/stop acquision"))
        self.actionStop_acquision.setText(_translate("MainWindow", "Stop acquision"))
        self.actionStart_stop_acquistion.setText(_translate("MainWindow", "Start/stop acquistion"))
        self.actionPreferences.setText(_translate("MainWindow", "Preferences..."))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

