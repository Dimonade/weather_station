import WeatherStationUI
import _dialogs.PreferencesUIView as PreferencesUIView
from WeatherStation import platform_type

import os
import sys
print("Updated recursion limit: {}.".format(sys.getrecursionlimit()))
import datetime
import configparser
import traceback
import pandas as pd
pd.set_option("display.max_columns", None)
if "Windows" in platform_type:
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
else:
    pass

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as mticker
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, QWidget, QSizePolicy
from PyQt5.QtCore import pyqtSignal


class WeatherStationUIView(QMainWindow):
    stopAcquisitionSignal = pyqtSignal(bool)
    
    def __init__(self, model, parent=None):
        super(WeatherStationUIView, self).__init__(parent)
        self.view = WeatherStationUI.Ui_MainWindow()
        self.view.setupUi(self)
        self.model = model
        self.setupUi(self)
        # self.view. = UI elements.
        # self. = Non-UI elements.
        return
    
    
    def setupUi(self, MainWindow):
        
        self.view.glayDevice00.removeWidget(self.view.widPlotDevice00)
        self.view.widPlotDevice00 = WeatherPlot(self.view.tabDevice00)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.view.widPlotDevice00.sizePolicy().hasHeightForWidth())
        self.view.widPlotDevice00.setSizePolicy(sizePolicy)
        self.view.widPlotDevice00.setObjectName("widPlotDevice00")
        self.view.glayDevice00.addWidget(self.view.widPlotDevice00, 0, 0, 1, 1)
        
        self.view.glaySystem.removeWidget(self.view.widPlotSystem)
        self.view.widPlotSystem = SystemPlot(self.view.tabSystem)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.view.widPlotSystem.sizePolicy().hasHeightForWidth())
        self.view.widPlotSystem.setSizePolicy(sizePolicy)
        self.view.widPlotSystem.setObjectName("widPlotSystem")
        self.view.glaySystem.addWidget(self.view.widPlotSystem, 0, 0, 1, 1)
        
        self.view.tabPlots.setCurrentIndex(0)
        
        self.preferences = dict({"Preferences":None,
                                 "PreferencesPath":os.path.join(os.getcwd(), "_preferences", "preferences.ini"),
                                 "AcquiredDataPath":None,
                                 "IsAcquiring":False,  # TODO: Will be used to pause/unpause the animation/acquistion.
                                 "Devices":list([])})
        self.LoadPreferencesFile()
        self.ScanDevices()
        
        return
    
    
    
    def LoadPreferencesFile(self):
        self.preferences["Preferences"] = configparser.ConfigParser()
        self.preferences["Preferences"].read(os.path.join(os.getcwd(), "_preferences", "preferences.ini"))
        return
    
    
    def DisplayPreferences(self):
        self.preferencesDialog = PreferencesUIView.PreferencesUIView(self)
        self.preferencesDialog.exec_()
        
        self.LoadPreferencesFile()
        if self.preferences["IsAcquiring"]:
            self.view.widPlotDevice00.GetPreferences(self.preferences)
        return
    
    
    def GetSavedMeasurementsLocation(self):
        print("Getting results saving location.")
        self.preferences["AcquiredDataPath"] = self.preferences["Preferences"].get("General", "data_save_location")
        print(self.preferences["AcquiredDataPath"])
        return
    
    
    def ScanDevices(self):
        self.model.VM_ScanDevices(self.preferences, self.UpdateDevices)
        return
    
    
    def UpdateDevices(self):
        self.preferences["Preferences"].set("General", "devices", "{}".format(self.preferences["Devices"]))
        
        if len(self.preferences["Devices"]) > 0:
            print("Found at least one.")
            self.preferences["Preferences"].set("Device00", "com", "{}".format(self.preferences["Devices"][0]))
        
        if len(self.preferences["Devices"]) > 1:
            print("Found at least two.")
            self.preferences["Preferences"].set("Device00", "com", "{}".format(self.preferences["Devices"][1]))
        
        
        with open(self.preferences["PreferencesPath"], "w+") as preferences:
            self.preferences["Preferences"].write(preferences)
        
        return
    
    
    def UpdateWeatherPlots(self):
        try:
            self.view.widPlotDevice00.UpdatePlots()
            self.view.widPlotSystem.UpdatePlots()
        except RecursionError:
            traceback.print_exc()
        except KeyboardInterrupt:
            traceback.print_exc()
        except:
            traceback.print_exc()
        return
    
    
    
    def GetMeasurements(self):
        self.model.VM_GetMeasurements(self.preferences, self.UpdateWeatherPlots)
        self.stopAcquisitionSignal.connect(self.model.GetMeasurementsThreadInstance.PauseUnpause)
    
    
    def StartStopAcquisition(self):
        self.preferences["IsAcquiring"] = not self.preferences["IsAcquiring"]
        print("StartStopAcquistion: Acquire: {}.".format(self.preferences["IsAcquiring"]))
        
        if self.preferences["IsAcquiring"]:
            self.GetSavedMeasurementsLocation()
            self.view.widPlotDevice00.GetPreferences(self.preferences)
            self.view.widPlotSystem.GetPreferences(self.preferences)
            self.GetMeasurements()
        
        else:
            self.stopAcquisitionSignal.emit(not self.preferences["IsAcquiring"])
        
        
        return


class SystemPlot(QWidget):
    def __init__(self, parent=None):
        super(SystemPlot, self).__init__(parent)
        
        self.figure = plt.figure(tight_layout=True)
        self.cpuTempAx = plt.subplot2grid((9,1), (0,0), rowspan=3, colspan=1)
        
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self, False)
        
        layout = QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        self.setLayout(layout)
        
        self.hours = mdates.HourLocator()
        self.hours.MAXTICKS = 2000
        self.hoursFmt = mdates.DateFormatter("%H")
        self.mins = mdates.MinuteLocator(byminute=[10, 20, 30, 40, 50])
        self.mins.MAXTICKS = 25000
        self.minsFmt = mdates.DateFormatter("%M")
        return
    
    
    def GetPreferences(self, preferences):
        print("GetPreferences: Got preferences successfully.")
        self.preferences = preferences
        return
    
    
    def AdjustHistory(self):
        cols = [i for i in self.preferences["Preferences"].get("System", "temperatures").split(",")]
        cols.insert(0, "DateTime")
        self.df = pd.read_csv(self.preferences["AcquiredDataPath"], parse_dates=["DateTime"], infer_datetime_format=True)
        self.df.set_index(["DateTime"], drop=True, inplace=True)
        
        h = float(self.preferences["Preferences"].get("General", "history"))
        start = (datetime.datetime.now() - datetime.timedelta(hours=h)).strftime("%Y/%m/%d %H:%M:%S")
        stop = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        
        self.df = self.df.loc[start:stop]
        self.df = self.df.round(1)
        
        if self.df.shape[0] > 2500*len(cols):
            print("Too many points to display, downscaling the plots to 2500 points per plot.")
            self.df = self.df[::round(2500*4/self.df.shape[0])]
        return
    
    
    def UpdatePlots(self):
        print("Updating plots.")
        self.AdjustHistory()
        
        if len(self.df.index) < 2:
            print("Dataframe is too small to be printed, waiting for more entries.")
            return
        
        strt = self.df.index[0]
        end = self.df.index[-1]
        
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()
        
        self.cpuTempAx.clear()
        
        # Temperature.
        self.cpuTempAx.set_ylabel("Temperature, [C]", fontsize="xx-small")
        self.cpuTempAx.grid(True, which="both", axis="both", alpha=0.5)
        self.cpuTempAx.yaxis.set_major_locator(mticker.MaxNLocator(nbins=4, prune="both"))
        self.cpuTempAx.spines["top"].set_visible(False)
        self.cpuTempAx.spines["right"].set_visible(False)
        self.cpuTempAx.spines["bottom"].set_visible(False)
        self.cpuTempAx.set_xlim(strt, end)
        
        self.cpuTempAx.axes.tick_params(axis="x", which="major", length=8, color="red", labelcolor="red", labelsize="small")
        self.cpuTempAx.axes.tick_params(axis="x", which="minor", labelsize="xx-small")
        self.cpuTempAx.tick_params(axis="y", which="both", labelsize="xx-small")
        self.cpuTempAx.xaxis.set_major_locator(self.hours)
        self.cpuTempAx.xaxis.set_major_formatter(self.hoursFmt)
        self.cpuTempAx.xaxis.set_minor_locator(self.mins)
        self.cpuTempAx.xaxis.set_minor_formatter(self.minsFmt)
        
        self.cpuTempAx.plot_date(self.df.index, self.df["CPU_Temperature"],
                             "g", marker=".", markersize=1, clip_on=True, linewidth=0)
        return


class WeatherPlot(QWidget):
    def __init__(self, parent=None):
        super(WeatherPlot, self).__init__(parent)
        
        # self.figure, (self.tempAx, self.pressureAx) = plt.subplots(2, sharex=True, squeeze=True)
        self.figure = plt.figure(tight_layout=True)
        self.tempAx = plt.subplot2grid((9,1), (0,0), rowspan=3, colspan=1)
        self.pressureAx = plt.subplot2grid((9,1), (3,0), rowspan=2, colspan=1, sharex=self.tempAx)
        self.humidityAx = plt.subplot2grid((9,1), (5,0), rowspan=2, colspan=1, sharex=self.tempAx)
        self.vocAx = plt.subplot2grid((9,1), (7,0), rowspan=2, colspan=1, sharex=self.tempAx)
        
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self, False)
        
        layout = QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        self.setLayout(layout)
        
        self.hours = mdates.HourLocator()
        self.hours.MAXTICKS = 2000
        self.hoursFmt = mdates.DateFormatter("%H")
        self.mins = mdates.MinuteLocator(byminute=[10, 20, 30, 40, 50])
        self.mins.MAXTICKS = 25000
        self.minsFmt = mdates.DateFormatter("%M")
        return
    
    
    def GetPreferences(self, preferences):
        print("GetPreferences: Got preferences successfully.")
        self.preferences = preferences
        return
    
    
    def AdjustHistory(self):
        cols = [i for i in self.preferences["Preferences"].get("Device00", "sensors").split(",") if i != "Illumination"]
        cols.insert(0, "DateTime")
        
        self.df = pd.read_csv(self.preferences["AcquiredDataPath"], parse_dates=["DateTime"], infer_datetime_format=True)
        self.df.set_index(["DateTime"], drop=True, inplace=True)
        
        h = float(self.preferences["Preferences"].get("General", "history"))
        start = (datetime.datetime.now() - datetime.timedelta(hours=h)).strftime("%Y/%m/%d %H:%M:%S")
        stop = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        
        self.df = self.df.loc[start:stop]
        self.df = self.df.round(1)
        print("History to display: {} [hours].".format(h))
        print("Start time: {}, stop time: {}.".format(start, stop))
        print("DataFrame: {}".format(self.df.tail()))
        
        if self.df.shape[0] > 2500*len(cols):  # Illumination isn't displayed.
            print("Too many points to display, downscaling the plots to 2500 points per plot.")
            self.df = self.df[::round(2500*4/self.df.shape[0])]
        return
    
    
    def UpdatePlots(self):
        print("Updating plots.")
        self.AdjustHistory()
        
        if len(self.df.index) < 2:
            print("Dataframe is too small to be printed, waiting for more entries.")
            return
        
        strt = self.df.index[0]
        end = self.df.index[-1]
        
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()
        
        self.tempAx.clear()
        self.pressureAx.clear()
        self.humidityAx.clear()
        self.vocAx.clear()
        
        # Temperature.
        self.tempAx.set_ylabel("Temperature, [C]", fontsize="xx-small")
        self.tempAx.grid(True, which="both", axis="both", alpha=0.5)
        self.tempAx.yaxis.set_major_locator(mticker.MaxNLocator(nbins=5, prune="both"))
        self.tempAx.tick_params(axis="x", which="both", bottom=False, top=False, labelbottom=False)
        self.tempAx.tick_params(axis="y", which="both", labelsize="xx-small")
        self.tempAx.spines["top"].set_visible(False)
        self.tempAx.spines["right"].set_visible(False)
        self.tempAx.spines["bottom"].set_visible(False)
        self.tempAx.set_xlim(strt, end)
        
        self.tempAx.plot_date(self.df.index, self.df["Temperature"],
                              "g", marker=".", markersize=1, clip_on=True, linewidth=0)
        
        # Pressure.
        self.pressureAx.set_ylabel("Pressure, [hPa]", fontsize="xx-small")
        self.pressureAx.grid(True, which="both", axis="both", alpha=0.5)
        self.pressureAx.yaxis.set_major_locator(mticker.MaxNLocator(nbins=3, prune="both"))
        self.pressureAx.tick_params(axis="x", which="both", bottom=False, top=False, labelbottom=False)
        self.pressureAx.tick_params(axis="y", which="both", labelsize="xx-small")
        self.pressureAx.spines["top"].set_visible(False)
        self.pressureAx.spines["right"].set_visible(False)
        self.pressureAx.spines["bottom"].set_visible(False)
        self.pressureAx.set_xlim(strt, end)
        
        self.pressureAx.plot_date(self.df.index, self.df["Pressure"],
                                  "r", marker=".", markersize=1, clip_on=True, linewidth=0)
        
        # Humidity.
        self.humidityAx.set_ylabel("Humidity, [%]", fontsize="xx-small")
        self.humidityAx.grid(True, which="both", axis="both", alpha=0.5)
        self.humidityAx.yaxis.set_major_locator(mticker.MaxNLocator(nbins=4, prune="both"))
        self.humidityAx.tick_params(axis="x", which="both", bottom=False, top=False, labelbottom=False)
        self.humidityAx.tick_params(axis="y", which="both", labelsize="xx-small")
        self.humidityAx.spines["top"].set_visible(False)
        self.humidityAx.spines["right"].set_visible(False)
        self.humidityAx.spines["bottom"].set_visible(False)
        self.humidityAx.set_xlim(strt, end)
        
        self.humidityAx.plot_date(self.df.index, self.df["Humidity"],
                                  "b", marker=".", markersize=1, clip_on=True, linewidth=0)
        
        # VOC.
        self.vocAx.set_ylabel("VOC, [*]", fontsize="xx-small")
        self.vocAx.grid(True, which="both", axis="both", alpha=0.5)
        self.vocAx.yaxis.set_major_locator(mticker.MaxNLocator(nbins=4, prune="both"))
        self.vocAx.spines["top"].set_visible(False)
        self.vocAx.spines["right"].set_visible(False)
        self.vocAx.spines["bottom"].set_visible(False)
        self.vocAx.set_xlim(strt, end)
        
        self.vocAx.axes.tick_params(axis="x", which="major", length=8, color="red", labelcolor="red", labelsize="small")
        self.vocAx.axes.tick_params(axis="x", which="minor", labelsize="xx-small")
        self.vocAx.tick_params(axis="y", which="both", labelsize="xx-small")
        self.vocAx.xaxis.set_major_locator(self.hours)
        self.vocAx.xaxis.set_major_formatter(self.hoursFmt)
        self.vocAx.xaxis.set_minor_locator(self.mins)
        self.vocAx.xaxis.set_minor_formatter(self.minsFmt)
        
        self.vocAx.plot_date(self.df.index, self.df["VOC"],
                             "y", marker=".", markersize=1, clip_on=True, linewidth=0)

        
        return
