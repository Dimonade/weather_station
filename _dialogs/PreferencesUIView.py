import os
import configparser

from PyQt5.QtWidgets import QDialog, QFileDialog
from PyQt5.QtCore import pyqtSignal

import _dialogs.PreferencesUI as PreferencesUI


class PreferencesUIView(QDialog):
    getSaveLocationSignal = pyqtSignal(object, object)
    
    def __init__(self, parent):
        super(PreferencesUIView, self).__init__(parent)
        self.view = PreferencesUI.Ui_Dialog()
        self.view.setupUi(self)
        # self.view. = UI elements.
        # self. = Non-UI elements.
        self.getSaveLocationSignal.connect(self.parent().GetSavedMeasurementsLocation)
        self.view.tabPreferences.setCurrentIndex(0)
        
        self.preferences = dict({"PreferencesPath":os.path.join(os.getcwd(), "_preferences", "preferences.ini"),
                                 "Preferences":configparser.ConfigParser(),
                                 })
        
        self.preferences["Preferences"].read(self.preferences["PreferencesPath"])
        
        self.PopulatePreferences()
        
        
        return
    
    
    def PopulatePreferences(self):
        # General preferences.
        
        savePath = os.path.join(os.path.expanduser("~/Desktop"))
        self.preferences["Preferences"].set("General", "data_save_location", "{}".format(savePath))
        self.view.leditDataSaveLocation.setText(savePath)
        
        history = self.preferences["Preferences"].get("General", "history")
        self.view.dsbHistory.setValue(float(history))
        
        file_shape = self.preferences["Preferences"].get("General", "file_shape")
        self.view.leditFileNameShape.setText(file_shape)
        
        plotUpdate = self.preferences["Preferences"].get("General", "plot_update")
        self.view.dsbPlotUpdate.setValue(float(plotUpdate))
        
        # Device 00.
        self.view.leditDevice00Name.setText(self.preferences["Preferences"].get("Device00", "name"))
        self.view.leditSensors00.setText(self.preferences["Preferences"].get("Device00", "sensors"))
        
        # System.
        self.view.leditTemperature.setText(self.preferences["Preferences"].get("System", "temperatures"))
        
        for com in self.preferences["Preferences"].get("General", "devices").split(","):
            self.view.cboxDevice00COM.addItem(com)
        
        self.SavePreferences()
        return
    
    
    def SetSaveResultsLocation(self):
        savePath = QFileDialog.getExistingDirectory(self, "Select saved data folder",
                                                    os.path.join(os.path.expanduser("~/Desktop")))
        if savePath == "":
            pass
        else:
            self.view.leditDataSaveLocation.setText(savePath)
        return
    
    
    def PreferencesUpdated(self):
        self.UpdateGeneral()
        self.UpdateDevice00()
        self.UpdateDevice01()
        
        self.SavePreferences()
        return
    
    
    def SavePreferences(self):
        with open(self.preferences["PreferencesPath"], "w+") as preferences:
            self.preferences["Preferences"].write(preferences)
        return
    
    
    def UpdateGeneral(self):
        self.preferences["Preferences"].set("General", 
                        "data_save_location", "{}".format(self.view.leditDataSaveLocation.text()))
        self.preferences["Preferences"].set("General", 
                        "history", "{}".format(self.view.dsbHistory.value()))
        return
    
    
    
    def UpdateDevice00(self):
        self.preferences["Preferences"].set("Device00", 
                        "name", "{}".format(self.view.leditDevice00Name.text()))
        self.preferences["Preferences"].set("Device00",
                        "sensors", "{}".format(self.view.leditSensors00.text()))
        return
    
    
    
    def UpdateDevice01(self):
        self.preferences["Preferences"].set("Device01", 
                        "name", "{}".format(self.view.leditDevice00Name.text()))
        self.preferences["Preferences"].set("Device01",
                        "sensors", "{}".format(self.view.leditSensors00.text()))
        
        
        return
